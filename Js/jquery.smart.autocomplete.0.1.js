(function($){

	$.fn.smartAutoComplete = function(collection, options) {

		var smartAutoComplete = function(el, data, params) {

			var defaultConfig = {},
				autoCompleteData = data || [],
				config = {},
				input = el || null;

			if(input == null) return;

			config = $.extend(defaultConfig, params);

			function S4() {
				return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
			}

			function guid() {
				return (S4()+S4()+S4()+S4()+S4()+S4()+S4()+S4());
			}

			function prepEl() {
				var wrapper = $('<div id="smartAutocomplete_'+ guid() +'" class="smartAutocomplete_wrapper" />');
				$(input).wrap(wrapper);

				$(input).css({
					visibility: 'hidden',
					position: 'absolute',
					top: 0,
					left: 0,
					zIndex: 1
				});

				$(input).parent().css({
					position: 'relative',
					width: $(input).width() + 5,
					height: $(input).height() + 5,
					border: '1px solid #CCC'
				});
			}

			function setup() {
				prepEl();
			}

			setup();
		};

		var data = collection || [],
			params = options || {},
			inputs = [];

		this.each(function(index, el) {
			inputs[index] = new smartAutoComplete(el, data, params);
		});

		return this;
	};

})(jQuery);